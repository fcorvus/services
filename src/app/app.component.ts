import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy } from '@angular/core';

import { ColorSchemeService } from './shared/color-scheme.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {
  title = 'services';
  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;
  theme: string;
  constructor(
    private colorSchemeService: ColorSchemeService,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher) {
    // Load Color Scheme
    this.colorSchemeService.load();
    this.theme = this.colorSchemeService.currentActive();
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);

  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  updateColorScheme(): void {
    this.theme = this.theme === 'dark' ? 'light' : 'dark';
    this.colorSchemeService.update(this.theme);
  }
}
